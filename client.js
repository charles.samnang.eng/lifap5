/* ******************************************************************
 * Constantes de configuration
 */
const apiKey = "dda60d7a-820e-4b74-a882-92c74c3d1303";
const serverUrl = "https://lifap5.univ-lyon1.fr";

/* ******************************************************************
 * Gestion des tabs "Voter" et "Toutes les citations"
 ******************************************************************** */

/**
 * Affiche/masque les divs "div-duel" et "div-tout"
 * selon le tab indiqué dans l'état courant.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majTab(etatCourant) {
  console.log("CALL majTab");
  const dDuel = document.getElementById("div-duel");
  const dTout = document.getElementById("div-tout");
  const tDuel = document.getElementById("tab-duel");
  const tTout = document.getElementById("tab-tout");
  if (etatCourant.tab === "duel") {
    dDuel.style.display = "flex";
    tDuel.classList.add("is-active");
    dTout.style.display = "none";
    tTout.classList.remove("is-active");
  } else {
    dTout.style.display = "flex";
    tTout.classList.add("is-active");
    dDuel.style.display = "none";
    tDuel.classList.remove("is-active");
  }
}

/**
 * Mets au besoin à jour l'état courant lors d'un click sur un tab.
 * En cas de mise à jour, déclenche une mise à jour de la page.
 *
 * @param {String} tab le nom du tab qui a été cliqué
 * @param {Etat} etatCourant l'état courant
 */
function clickTab(tab, etatCourant) {
  console.log(`CALL clickTab(${tab},...)`);
  if (etatCourant.tab !== tab) {
    etatCourant.tab = tab;
    majPage(etatCourant);
  }
  if(etatCourant.tab === 'tout')
  {
	  lanceFetchCitationsEtInsereCitations(etatCourant);
  }
  else if(etatCourant.tab ==='duel')
  {
	  majDuelAleatoire(etatCourant);
  }
}

/**
 * Enregistre les fonctions à utiliser lorsque l'on clique
 * sur un des tabs.
 *
 * @param {Etat} etatCourant l'état courant
 */
function registerTabClick(etatCourant) {
  console.log("CALL registerTabClick");
  document.getElementById("tab-duel").onclick = () =>
    clickTab("duel", etatCourant);
  document.getElementById("tab-tout").onclick = () =>
    clickTab("tout", etatCourant);
}

/* ******************************************************************
 * Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
 * l'utilisateur.
 * ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /whoami
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami() {
  return fetch(serverUrl + "/whoami", { headers: { "x-api-key": apiKey } })
    .then((response) => response.json())
    .then((jsonData) => {
      if (jsonData.status && Number(jsonData.status) != 200) {
        return { err: jsonData.message };
      }
      return jsonData;
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête GET authentifiée sur /whoami
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami(clefAPI) {
  return fetch(serverUrl + "/whoami", { headers: { "x-api-key": clefAPI } })
    .then((response) => response.json())
    .then((jsonData) => {
      if (jsonData.status && Number(jsonData.status) != 200) {
        return { err: jsonData.message };
      }
      return jsonData;
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur et insère le login dans
 * la modale d'affichage de l'utilisateur.
 *
 * @param {Etat} etatCourant l'état courant
 * @returns Une promesse de mise à jour
 */
function lanceWhoamiEtInsereLogin(etatCourant) {
  return fetchWhoami().then((data) => {
    etatCourant.login = data.login; // qui vaut undefined en cas d'erreur
    etatCourant.errLogin = data.err; // qui vaut undefined si tout va bien
    majPage(etatCourant);
    // Une promesse doit renvoyer une valeur, mais celle-ci n'est pas importante
    // ici car la valeur de cette promesse n'est pas utilisée. On renvoie
    // arbitrairement true
    return true;
  });
}

/**
 * Affiche ou masque la fenêtre modale de login en fonction de l'état courant.
 * Change la valeur du texte affiché en fonction de l'état
 *
 * @param {Etat} etatCourant l'état courant
 */
function majModalLogin(etatCourant) {
  const modalClasses = document.getElementById("mdl-login").classList;
  if (etatCourant.loginModal) {
    modalClasses.add("is-active");
  } else {
    modalClasses.remove("is-active");
  }
}

/**
 * Déclenche l'affichage de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickFermeModalLogin(etatCourant) {
  etatCourant.loginModal = false;
  majPage(etatCourant);
}

/**
 * Déclenche la fermeture de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickOuvreModalLogin(etatCourant) {
  etatCourant.loginModal = true;
  majPage(etatCourant);
}

/**
 * Vérifie que la clé API est bonne, et si c'est le cas, change l'affichage
 * des boutons et affiche l'utilisateur
 * @param {Etat} etatCourant l'état courant
 */
function connexion(etatCourant)
{
	const clefAPI = document.getElementById("api").value;
	const deconnexion = document.getElementById("deconnexion").classList;
	const connexion = document.getElementById("btn-open-login-modal").classList;
	const utilisateur = document.getElementById("utilisateur");
	return fetchWhoami(clefAPI).then((data) => {
		if(data.login === undefined)
		{
			alert("La clef API n'est pas bonne");
		}
		else
		{
			deconnexion.remove("is-hidden");
			connexion.add("is-hidden");
			utilisateur.textContent = data.login;
			clickFermeModalLogin(etatCourant);
		}
	});
}

/**
 * Remet à zéro le champ de la clé API et change l'affichage des boutons
 *
 * @param {Etat} etatCourant l'état courant
 */
function deconnexion(etatCourant)
{
	document.getElementById("api").value = "";
	const deconnexion = document.getElementById("deconnexion").classList;
	const connexion = document.getElementById("btn-open-login-modal").classList;
	const utilisateur = document.getElementById("utilisateur");
	utilisateur.textContent = "";
	deconnexion.add("is-hidden");
	connexion.remove("is-hidden");
}

/**
 * Enregistre les actions à effectuer lors d'un click sur les boutons
 * d'ouverture/fermeture de la boîte de dialogue affichant l'utilisateur.
 * @param {Etat} etatCourant
 */
function registerLoginModalClick(etatCourant) {
  document.getElementById("btn-close-login-modal1").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("connexion").onclick = () =>
    connexion(etatCourant);
  document.getElementById("deconnexion").onclick = () =>
	deconnexion(etatCourant);
  document.getElementById("btn-open-login-modal").onclick = () =>
    clickOuvreModalLogin(etatCourant);
}



/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  console.log("CALL majPage");
  majTab(etatCourant);
  majModalLogin(etatCourant);
  majCitations(etatCourant);
  majDuelAleatoire(etatCourant);
  registerTabClick(etatCourant);
  registerLoginModalClick(etatCourant);
  registerCitationModalClick();
}

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */
function initClientCitations() {
  console.log("CALL initClientCitations");
  const etatInitial = {
    tab: "duel",
    loginModal: false,
    login: undefined,
    errLogin: undefined,
    citations: {}
  };
  lanceFetchCitationsEtInsereCitations(etatInitial);
  majPage(etatInitial);
}

// Appel de la fonction init_client_duels au après chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  console.log("Exécution du code après chargement de la page");
  initClientCitations();
});

/* ******************************************************************
 * Gestion des citations et de leurs affichages
 * ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /citations
 * @returns une promesse de toutes les citations dans la base de données ou du message d'erreur
 */
function fetchCitations() {
  return fetch(serverUrl + "/citations", { headers: { "x-api-key": apiKey } })
    .then((response) => response.json())
    .then((jsonData) => {
      if (jsonData.status && Number(jsonData.status) != 200) {
        return { err: jsonData.message };
      }
      return jsonData;
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur et insère les citations dans l'étatCourant.
 *
 * @param {Etat} etatCourant l'état courant
 * @returns Une promesse de mise à jour
 */
function lanceFetchCitationsEtInsereCitations(etatCourant){
	return fetchCitations().then((data) => {
    etatCourant.citations = data; // qui vaut undefined en cas d'erreur
    majPage(etatCourant);
    return true;
  });
}

/**
 * Met à jour le tableau des citations
 *
 * @param {Etat} etatCourant l'état courant
 */
function majCitations(etatCourant)
{
	const tableau = document.querySelector('tbody');
	tableau.innerHTML = "";
	var i=0;
	const tableauCitations = etatCourant.citations.map(
		citationActuelle => {
			i++;
			const ligneActuelle = document.createElement('tr');
			const caseClassement = document.createElement('th');
			const casePersonnage = document.createElement('td');
			const caseCitation = document.createElement('td');
			caseClassement.textContent = i;
			casePersonnage.textContent = citationActuelle.character;
			caseCitation.textContent = citationActuelle.quote;
			ligneActuelle.appendChild(caseClassement);
			ligneActuelle.appendChild(casePersonnage);
			ligneActuelle.appendChild(caseCitation);
			ligneActuelle.onclick = () => afficherDetailCitation(citationActuelle);
			tableau.appendChild(ligneActuelle);
		});
}

/**
 * Permet de générer un nombre aléatoire entre le minimum et le maximum
 *
 * @param {min, max} min et max étant respectivement le minimum et le maximum
 * @returns un nombre aléatoire compris entre min et max inclus
 */
function genererNbAleatoire(min,max)
{
	return Math.floor(Math.random()*(max-min+1))+min;
}

/**
 * Permet de générer un nombre aléatoire entre le minimum et le maximum, avec un nombre interdit dans l'intervalle
 *
 * @param {min, max, numCitation1} min et max étant respectivement le minimum et le maximum. numCitation1 correspond au numéro de la 1ère citation aléatoire
 * @returns un nombre aléatoire compris entre min et max inclus, en excluant la valeur de numCitation1
 */
function genererNbAleatoire2(min,max, numCitation1)
{
	const nbAleatoire = Math.floor(Math.random()*(max-min+1))+min;
	return (nbAleatoire === numCitation1) ? genererNbAleatoire2(min, max, numCitation1) : nbAleatoire;
}

/**
 * Créer un tableau contenant 2 citations aléatoire différentes et le retourne
 *
 * @param {Etat} etatCourant l'état courant
 * @returns un tableau contenant 2 citations aléatoires différentes
 */
function choixCitationAleatoire(etatCourant)
{
	const nbCitations = etatCourant.citations.length;
	const nbAleatoire = genererNbAleatoire(0,nbCitations);
	const nbAleatoire2 = genererNbAleatoire2(0,nbCitations, nbAleatoire);
	const tab2Citations = [etatCourant.citations[nbAleatoire], etatCourant.citations[nbAleatoire2]];
	return(tab2Citations);
}

/**
 * Met à jour les 2 citations de la div duels.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majDuelAleatoire(etatCourant)
{
	const citations = choixCitationAleatoire(etatCourant);
	const imageGauche = document.querySelectorAll('img')[0];
	const imageDroite = document.querySelectorAll('img')[1];
	imageGauche.src = citations[0].image;
	if(citations[0].characterDirection === 'Right') { imageGauche.style = "transform: scaleX(-1)"; }
	else imageGauche.style = "";
	imageDroite.src = citations[1].image;
	if(citations[1].characterDirection === 'Left') {imageDroite.style = "transform: scaleX(-1)";}
	else imageDroite.style = "";
	const auteurCitationGauche = document.querySelectorAll('.subtitle')[1];
	const auteurCitationDroite = document.querySelectorAll('.subtitle')[2];
	auteurCitationGauche.textContent = citations[0].character + ' dans ' + citations[0].origin;
	auteurCitationDroite.textContent = citations[1].character + ' dans ' + citations[1].origin;
	const citationGauche = document.querySelectorAll('.title')[1];
	const citationDroite = document.querySelectorAll('.title')[2];
	citationGauche.textContent = citations[0].quote;
	citationDroite.textContent = citations[1].quote;
	const boutonGauche = document.getElementById("boutonVoteGauche");
	boutonGauche.onclick = () => voteDuel(citations[0], citations[1]);
	const boutonDroit = document.getElementById("boutonVoteDroit");
	boutonDroit.onclick = () => voteDuel(citations[1], citations[0]);
}

/**
 * Permet d'envoyer le résultat du duel entre les 2 citations.
 *
 * @param {citationGagnante, citationPerdante} citationGagnante correspondant à la citation gagnante, idem pour citationPerdante.
 */
function voteDuel(citationGagnante, citationPerdante)
{
	const idGagnante = citationGagnante._id;
	const idPerdante = citationPerdante._id;
	fetch(serverUrl + "/citations/duels", { method: "POST", headers : { "x-api-key": apiKey, "Content-type": "application/json" }, body: JSON.stringify({"winner": idGagnante, "looser": idPerdante})})
	.then((response) => response.json())
	.then(function(data){
		console.log("Vote ajouté !");
	})
}

/* ******************************************************************
 * Gestion de la fenêtre modale des détails des citations
 * ****************************************************************** */

/**
 * Met à jour les champs HTML correspondant en fonction de la citation cliquée.
 *
 * @param {citation} citation étant la citation sur laquelle l'utilisateur a cliqué.
 */
function afficherDetailCitation(citation)
{
	const modalClasses = document.getElementById("modal-citation").classList;
    modalClasses.add("is-active");
    const imageCitation = document.getElementById("imgCitation");
    const citationActuelle = document.getElementById("citation");
    const personnage = document.getElementById("personnage");
    const origine = document.getElementById("origine");
    const direction = document.getElementById("direction");
    const auteur = document.getElementById("auteur");
    imageCitation.src = citation.image;
    citationActuelle.textContent = citation.quote;
    personnage.textContent = citation.character;
    origine.textContent = citation.origin;
    direction.textContent = citation.characterDirection;
    auteur.textContent = citation.addedBy;
}

/**
 * Ferme la fenêtre modale des détails de la citation en enlevant la classe 'is-active'.
 *
 */
function fermerDetailCitation()
{
	const modalClasses = document.getElementById("modal-citation").classList;
	modalClasses.remove("is-active");
}

/**
 * Enregistre les actions à effectuer lors d'un click sur les boutons
 * d'ouverture/fermeture de la boîte de dialogue affichant les détails d'une citation
 */
function registerCitationModalClick() {
  document.getElementById("btn-close-citation-modal").onclick = () =>
    fermerDetailCitation();
  document.getElementById("btn-close-citation-modal2").onclick = () =>
    fermerDetailCitation();
}

/* ******************************************************************
 * Gestion de la boîte de dialogue (a.k.a. modal) d'affichage du
 * formulaire d'ajout de citations
 * ****************************************************************** */

/**
 * Remet à zéro tous les champs du formulaire d'ajout de citation
 */
function razFormulaire()
{
	document.getElementById("texteAjoutCitation").value = "";
	document.getElementById("personnageAjoutCitation").value = "";
	document.getElementById("imageAjoutCitation").value = ""
	document.getElementById("directionAjoutCitation").value = "";
	document.getElementById("origineAjoutCitation").value = "";
}

document.getElementById("boutonAjoutCitation").onclick = () => submitAjoutCitation();
document.getElementById("ajouterCitation").onclick = () => afficherFormulaireAjout();
document.getElementById("btn-fermerAjoutCitation").onclick = () => fermerFormulaireAjout();

/**
 * Fait apparaître la fenêtre modale du formulaire d'ajout de citation en ajoutant la classe 'is-active'
 */
function afficherFormulaireAjout()
{
	const modalClasses = document.getElementById("modal-ajoutCitation").classList;
	modalClasses.add("is-active");
}

/**
 * Cache la fenêtre modale du formulaire d'ajout de citation en enlevant la classe 'is-active'
 */
function fermerFormulaireAjout()
{
	const modalClasses = document.getElementById("modal-ajoutCitation").classList;
	modalClasses.remove("is-active");
}

/**
 * Appel la fonction d'ajout de citation dans le serveur si les champs obligatoires sont bien remplis
 */
function submitAjoutCitation()
{
	const citation = document.getElementById("texteAjoutCitation").value;
	const personnage = document.getElementById("personnageAjoutCitation").value;
	const image = document.getElementById("imageAjoutCitation").value;
	const direction = document.getElementById("directionAjoutCitation").value;
	const origine = document.getElementById("origineAjoutCitation").value;
	if(citation && personnage && origin)
	{
		creerCitationAjoutee({
		quote: citation,
		character: personnage,
		image: image,
		characterDirection: direction,
		origin: origine
		});
		razFormulaire();
	}
	else
	{
		alert("Veuillez remplir tous les champs obligatoires");
	}
}

/**
 * Ajoute la citation donnée dans la base de données.
 *
 * @param {citation} citation la citation envoyée pour être ajoutée
 */
function creerCitationAjoutee(citation)
{
	fetch(serverUrl + "/citations", { method:'POST', body:JSON.stringify(citation), headers: { "Content-type": "application/json", "x-api-key": apiKey}})
	.then((response) => response.json())
	.then(function(data){
		console.log('Citation Ajoutée !');
	})
}
