// Tests unitaires pour le Projet

suite("Tests pour la fonction choixCitationAleatoire",
	function() {
		
		test("On vérifie que les 2 nombres aléatoires reçus sont différents",
			function() {
				const nb1 = genererNbAleatoire(0,1);
				const nb2 = genererNbAleatoire2(0,1,nb1);
				chai.assert.notEqual(nb1,nb2);
			});
			
		test("On vérifie que les 2 citations sont différentes",
			function() {
				const etatTest = { citations:[{
				  "_id": "608e5c0202ab7b2931ed334c",
				  "quote": "That's where I saw the leprechaun...He told me to burn things.",
				  "character": "Ralph Wiggum",
				  "image": "https://cdn.glitch.com/3c3ffadc-3406-4440-bb95-d40ec8fcde72%2FRalphWiggum.png?1497567511523",
				  "characterDirection": "Left",
				  "origin": "The Simpsons",
				  "__v": 0
				},
				{
				  "_id": "608e5c0202ab7b2931ed3345",
				  "quote": "Last night's \"Itchy & Scratchy\" was, without a doubt, the worst episode ever. Rest assured that I was on the Internet within minutes, registering my disgust throughout the world.",
				  "character": "Comic Book Guy",
				  "image": "https://cdn.glitch.com/3c3ffadc-3406-4440-bb95-d40ec8fcde72%2FComicBookGuy.png?1497567511970",
				  "characterDirection": "Right",
				  "origin": "The Simpsons",
				  "__v": 0
				}
			]};
			const tabCitations = choixCitationAleatoire(etatTest);
			chai.assert.notEqual(tabCitations[0], tabCitations[1]);
		});
	});

suite("Tests pour la fonction fetchWhoAmI",
	function() {
		
		test("On vérifie qu'on fetch le bon numéro étudiant",
			function() {
				
				fetchWhoami().then((data) => {
				chai.assert.equal(data.login,"p1403762");
				});
				
			});
		});
